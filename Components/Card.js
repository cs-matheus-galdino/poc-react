import React from 'react';

const Card = (props) => {
  
  return (
  	<div key={props.id}>
      <img width="100" src={props.repositorio.avatar_url} />
      <div>{props.repositorio.name}</div>
      <div>{props.repositorio.company}</div>
    </div>
	);
};

export default Card;
