import React, { Component } from 'react';

import Card from './Card';

// Functional component => Usado quando você não armazena estado do componente.
const CardList = (props) => {
  return (
    <div>
      {
        props.githubRepos.map((repository, index) => {
          return <Card key={index} repositorio={repository} />;
        } )
      }
    </div>
  );
};

export default CardList;
