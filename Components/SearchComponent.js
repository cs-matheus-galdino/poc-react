import React from 'react';
import axios from 'axios';

class SearchComponent extends React.Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit = (event) => {
    event.preventDefault();
    
    axios.get(`https://api.github.com/users/${this.userNameInput.value}`)
      .then(response => this.props.updateGithubList(response.data))
      .catch(error => console.log(this.userNameInput.value));
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input ref={(input) => this.userNameInput = input} placeholder='Github username' type="text" />
        <button type='submit'>Add card</button>
      </form>
    );
  }
}

export default SearchComponent;
