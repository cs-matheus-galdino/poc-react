import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

// Componentes
import CardList from './Components/CardList';
import SearchComponent from './Components/SearchComponent';

// Class-based component, usado quando você armazena estado
class App extends React.Component {
  constructor() {
    super();

    this.state = {
      githubRepos: []
    }

    this.updateGithubList = this.updateGithubList.bind(this);
  }
  
  updateGithubList(repository) {
    if (!this.state.githubRepos.find(repo => repo.name === repository.name)) {
      this.setState((prevState) => ({
        githubRepos: prevState.githubRepos.concat(repository)
      }));
    }
  }

  componentDidMount() {
    this.setState({
      githubRepos: [{
        "login": "barbaromatrix",
        "id": 13008245,
        "avatar_url": "https://avatars0.githubusercontent.com/u/13008245?v=4",
        "name": "Matheus",
        "company": "@concretesolutions",
      }]
    })
  }

  render() {
    return (
      <div>
        <SearchComponent githubRepos={this.state.githubRepos} updateGithubList={this.updateGithubList} />

        <CardList githubRepos={this.state.githubRepos} />
      </div>
    );
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);